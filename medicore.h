#ifndef MEDICORE_H
#define MEDICORE_H

#include <QObject>
#include <QImage>

template <typename T>
class QFutureWatcher;

class MediCore : public QObject
{
	Q_OBJECT
public:
	explicit MediCore(QObject *parent = 0);

	bool loadImage(const QString &path);

	const QImage &filteredImage() const { return m_filtered; }

	const QImage &originalImage() const { return m_image; }

signals:
	void finished();
	void progressRangeChanged(int, int);
	void progressValueChanged(int);

public slots:
	void startMeanFilter();
	void startMedianFilter();
	void abort();

private:
	QVector<int> m_vScanLines;
	QFutureWatcher<void> *m_watcher;
	QImage m_image;
	QImage m_filtered;
};

#endif // MEDICORE_H
