#include "medicore.h"
#include <QFutureWatcher>
#include <QtConcurrent>

// const int wndSize = 3;
// const int wnd[wndSize][wndSize] = {
// //	{ 1, 1, 1 },
// //	{ 1, 2, 1 },
// //	{ 1, 1, 1 }
// //	{ 0, 0, 0 },
// //	{ 0, 9, 0 },
// //	{ 0, 0, 0 }
// 	{ 1, 1, 1 },
// 	{ 1, 1, 1 },
// 	{ 1, 1, 1 }
// //	{ 2, 0, 2 },
// //	{ 0, 1, 0 },
// //	{ 2, 0, 2 }
// };

const int wndSize = 9;
const int wndMean[wndSize][wndSize] = {
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1 }
};

MediCore::MediCore(QObject *parent) : QObject(parent)
{
	m_watcher = new QFutureWatcher<void>(this);
	connect(m_watcher, SIGNAL(finished()), this, SIGNAL(finished()));
	connect(m_watcher, SIGNAL(progressRangeChanged(int, int)),
			this, SIGNAL(progressRangeChanged(int,int)));
	connect(m_watcher, SIGNAL(progressValueChanged(int)),
			this, SIGNAL(progressValueChanged(int)));
}

bool MediCore::loadImage(const QString &path)
{
	if (!m_image.load(path))
		return false;

	m_image = m_image.convertToFormat(QImage::Format_RGB888);
	m_filtered = QImage(m_image.size(), m_image.format());
	return true;
}

struct MeanFunc
{
	QImage * imageOriginal;
	QImage * imageFiltered;
	MeanFunc(QImage *original, QImage *filtered) :
		imageOriginal(original), imageFiltered(filtered) {}

	void operator()(int y)
	{
		static const int Bpp = 3;
		static const int wndFace = wndSize*wndSize;
		const int width = imageOriginal->width();
		const int height = imageOriginal->height();

		// initialize srcScanLines.
		uchar* dstScanLine = imageFiltered->scanLine(y);
		const uchar* srcScanLine[wndSize];
		for (int s = 0; s < wndSize; ++s)
		{
			const int w = s - (wndSize/2);
			srcScanLine[s] = imageOriginal->constScanLine(qMax(0, qMin(height - 1, y + w)));
		}

		// calc the mean and set the filtered pixel.
		for (int x = 0; x < width; ++x)
		{
			const int xOffset = x*Bpp;
			int sumR = 0;
			int sumG = 0;
			int sumB = 0;
			for (int wy = 0; wy < wndSize; ++wy)
			{
				const uchar* srcLine = srcScanLine[wy];
				for (int wx = 0; wx < wndSize; ++wx)
				{
					const int w = wx - (wndSize/2);
					const uchar *srcOffset = srcLine + qMax(0, qMin(width - 1, x + w)) * Bpp;
					sumR += wndMean[wx][wy]*srcOffset[0];
					sumG += wndMean[wx][wy]*srcOffset[1];
					sumB += wndMean[wx][wy]*srcOffset[2];
				}
			}
			dstScanLine[xOffset]     = uchar(sumR / wndFace);
			dstScanLine[xOffset + 1] = uchar(sumG / wndFace);
			dstScanLine[xOffset + 2] = uchar(sumB / wndFace);
		}
	}
};

struct MedianFunc
{
	QImage * imageOriginal;
	QImage * imageFiltered;
	MedianFunc(QImage *original, QImage *filtered) :
		imageOriginal(original), imageFiltered(filtered) {}

	struct RGB {
		RGB(uchar r = 0, uchar g = 0, uchar b = 0) : red(r), green(g), blue(b) {}
		uchar red, green, blue, a; // some padding improves performance
		bool operator < (const RGB &other) {
			return ((int)red + green + blue) < ((int) other.red + other.green + other.blue);
		}
	};

	void operator()(int y)
	{
		static const int Bpp = 3;
		static const int wndFace = wndSize*wndSize;
		const int width = imageOriginal->width();
		const int height = imageOriginal->height();
		RGB colors[wndFace];

		// initialize srcScanLines.
		uchar* dstScanLine = imageFiltered->scanLine(y);
		const uchar* srcScanLine[wndSize];
		for (int s = 0; s < wndSize; ++s)
		{
			const int w = s - (wndSize/2);
			srcScanLine[s] = imageOriginal->constScanLine(qMax(0, qMin(height - 1, y + w)));
		}

		// calc the median and set the filtered pixel.
		for (int x = 0; x < width; ++x)
		{
			const int xOffset = x*Bpp;
			for (int wy = 0, i = 0; wy < wndSize; ++wy)
			{
				const uchar* srcLine = srcScanLine[wy];
				for (int wx = 0; wx < wndSize; ++wx, ++i)
				{
					const int w = wx - (wndSize/2);
					const uchar *srcOffset = srcLine + qMax(0, qMin(width - 1, x + w)) * Bpp;
					colors[i].red = srcOffset[0];
					colors[i].green = srcOffset[1];
					colors[i].blue = srcOffset[2];
				}
			}
			std::sort(&colors[0], &colors[wndFace]);
			const RGB &colorMedian = colors[wndFace/2];
			dstScanLine[xOffset]     = colorMedian.red;
			dstScanLine[xOffset + 1] = colorMedian.green;
			dstScanLine[xOffset + 2] = colorMedian.blue;
		}
	}
};

void MediCore::startMeanFilter()
{
	if (m_watcher->future().isRunning())
	{
		qWarning()<<"Trying to run filter while running.";
		return;
	}

//	QThreadPool::globalInstance()->setMaxThreadCount(1);
	// Fill vector with scan line indexes.
	const int height = m_filtered.height();
	m_vScanLines.resize(height);
	for (int y = 0; y < height; ++y)
		m_vScanLines[y] = y;

	m_watcher->setFuture(QtConcurrent::map(m_vScanLines, MeanFunc(&m_image, &m_filtered)));
}

void MediCore::startMedianFilter()
{
	if (m_watcher->future().isRunning())
	{
		qWarning()<<"Trying to run filter while running.";
		return;
	}

//	QThreadPool::globalInstance()->setMaxThreadCount(1);
	// Fill vector with scan line indexes.
	const int height = m_filtered.height();
	m_vScanLines.resize(height);
	for (int y = 0; y < height; ++y)
		m_vScanLines[y] = y;

	m_watcher->setFuture(QtConcurrent::map(m_vScanLines, MedianFunc(&m_image, &m_filtered)));
}


void MediCore::abort()
{
	m_watcher->cancel();
}
