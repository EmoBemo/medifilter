#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QSplitter>
#include <QScrollArea>
#include <QScrollBar>
#include <QLabel>
#include <QSettings>
#include "medicore.h"
#include <QElapsedTimer>

QString FormatTime_ms(const quint64 &ms)
{
	quint64 nHours, nMinutes, nSeconds, nMiliSeconds;
	nHours = ms / 3600000;
	quint64 rem = ms % 3600000;
	nMinutes = (rem) / 60000;
	rem = rem % 60000;
	nSeconds = (rem) / 1000;
	rem = (rem) % 1000;
	nMiliSeconds = rem;

	return QString("%1:%2:%3.%4").
		arg(nHours, 2, 10, QChar('0')).
		arg(nMinutes, 2, 10, QChar('0')).
		arg(nSeconds, 2, 10, QChar('0')).
		arg(nMiliSeconds, 3, 10, QChar('0'));
}

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	m_progressDlg(0)
{
	ui->setupUi(this);
	setWindowTitle(qApp->applicationName());
	m_core = new MediCore(this);
	m_elapsedTimer.reset(new QElapsedTimer);

	connect(ui->action_Load, SIGNAL(triggered(bool)), this, SLOT(loadImage()));
	connect(ui->action_MeanFilter, SIGNAL(triggered(bool)), this, SLOT(startMeanFilter()));
	connect(ui->actionM_edian_Filter, SIGNAL(triggered(bool)), this, SLOT(startMedianFilter()));
	connect(m_core, SIGNAL(finished()), this, SLOT(updateFilteredImage()));
	connect(m_core, SIGNAL(finished()), this, SLOT(updateElapsedTime()));

	connect(ui->areaOriginal->horizontalScrollBar(), SIGNAL(valueChanged(int)),
			ui->areaFiltered->horizontalScrollBar(), SLOT(setValue(int)));
	connect(ui->areaFiltered->horizontalScrollBar(), SIGNAL(valueChanged(int)),
			ui->areaOriginal->horizontalScrollBar(), SLOT(setValue(int)));
	connect(ui->areaOriginal->verticalScrollBar(), SIGNAL(valueChanged(int)),
			ui->areaFiltered->verticalScrollBar(), SLOT(setValue(int)));
	connect(ui->areaFiltered->verticalScrollBar(), SIGNAL(valueChanged(int)),
			ui->areaOriginal->verticalScrollBar(), SLOT(setValue(int)));
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::loadImage()
{
	QSettings settings;

	const QString &path =
		QFileDialog::getOpenFileName(this, tr("Load"), settings.value("LoadPath").toString());

	if (path.isEmpty())
		return;

	if (!m_core->loadImage(path))
	{
		QMessageBox::critical(this, tr("Error"), tr("Failed to load image"));
		return;
	}

	updateOriginalImage();
	updateFilteredImage();
	settings.setValue("LoadPath", path);
	setWindowTitle(qApp->applicationName() + " - " + QFileInfo(path).fileName());
}

void MainWindow::startMeanFilter()
{
	prepareProgress();
	m_progressDlg->show();
	m_elapsedTimer->start();
	m_core->startMeanFilter();
	statusBar()->showMessage("Mean filtering...");
}

void MainWindow::startMedianFilter()
{
	prepareProgress();
	m_progressDlg->show();
	m_elapsedTimer->start();
	m_core->startMedianFilter();
	statusBar()->showMessage("Median filtering...");
}

void MainWindow::prepareProgress()
{
	if (!m_progressDlg)
	{
		m_progressDlg = new QProgressDialog(this);
		connect(m_core, SIGNAL(finished()), m_progressDlg, SLOT(hide()));
		connect(m_core, SIGNAL(progressRangeChanged(int,int)),
			m_progressDlg, SLOT(setRange(int,int)));
		connect(m_core, SIGNAL(progressValueChanged(int)),
			m_progressDlg, SLOT(setValue(int)));
		connect(m_progressDlg, SIGNAL(canceled()),
			m_core, SLOT(abort()));
	}
}

void MainWindow::updateFilteredImage()
{
	QLabel * imgLabel = new QLabel(ui->areaFiltered);
	imgLabel->setPixmap(QPixmap::fromImage(m_core->filteredImage()));	
	ui->areaFiltered->setWidget(imgLabel);
}

void MainWindow::updateOriginalImage()
{
	QLabel * imgLabel = new QLabel(ui->areaOriginal);
	imgLabel->setPixmap(QPixmap::fromImage(m_core->originalImage()));
	ui->areaOriginal->setWidget(imgLabel);
}

void MainWindow::updateElapsedTime()
{
	statusBar()->showMessage(FormatTime_ms(m_elapsedTimer->elapsed()));
}
