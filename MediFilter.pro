#-------------------------------------------------
#
# Project created by QtCreator 2015-12-13T20:48:36
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MediFilter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    medicore.cpp

HEADERS  += mainwindow.h \
    medicore.h

FORMS    += mainwindow.ui
