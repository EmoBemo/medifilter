#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScopedPointer>

namespace Ui {
class MainWindow;
}

class MediCore;
class QProgressDialog;
class QLabel;
class QElapsedTimer;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void loadImage();
	void startMeanFilter();
	void startMedianFilter();
	void updateFilteredImage();
	void updateOriginalImage();
	void updateElapsedTime();

private:
	void prepareProgress();

private:
	Ui::MainWindow *ui;
	MediCore *m_core;
	QProgressDialog *m_progressDlg;
	QScopedPointer<QElapsedTimer> m_elapsedTimer;
};

#endif // MAINWINDOW_H
