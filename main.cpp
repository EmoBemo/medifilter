#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	a.setOrganizationName("Medi");
	a.setApplicationName("MediFilter");
	MainWindow w;
	w.show();

	return a.exec();
}
